import React, {Component} from 'react';
import styles from './ShowMoreBlock.css';

class ShowMoreBlock extends Component {

    render() {
        return(
            <div className="showMoreBlock">
                <button className="showMoreBlock_button">Show More</button>
            </div>
        )
    }
}

export default ShowMoreBlock;