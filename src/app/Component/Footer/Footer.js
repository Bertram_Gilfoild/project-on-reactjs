import React, {Component} from 'react';
import styles from './Footer.css';
import FooterAboutUs from "./Subcomponent/FooterAboutUs/FooterAboutUs";
import FooterDescription from "./Subcomponent/FooterDescription/FooterDescription";


class Footer extends Component {

    render() {
        return(
            <div className="footer">
                <div className="footerBlock">
                    <FooterAboutUs/>
                    <FooterDescription/>
                </div>
            </div>
        )
    }
}

export default Footer;