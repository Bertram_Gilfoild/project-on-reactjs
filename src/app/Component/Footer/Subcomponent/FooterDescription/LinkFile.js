import React, {Component} from 'react';

const Featured=(props)=>{
    return <a>Featured</a>
};
const Furniture=(props)=>{
    return <a>Furniture</a>
};
const Decoration=(props)=>{
    return <a>Decoration</a>
};
const Electronics=(props)=>{
    return <a>Electronics</a>
};
const Lighting=(props)=>{
    return <a>Lighting</a>
};
const AboutUs=(props)=>{
    return <a>About Us</a>
};
const ContactUs=(props)=>{
    return <a>Contact Us</a>
};
const Delivery=(props)=>{
    return <a>Delivery</a>
};
const Returns=(props)=>{
    return <a>Returns</a>
};

class LinkFile extends Component {

    render() {

        const {type} = this.props;
        const mapTypeToLink = {
            featured: ()=> <Featured/>,
            furniture: ()=> <Furniture/>,
            decoration: ()=> <Decoration/>,
            electronics: ()=> <Electronics/>,
            lighting: ()=>  <Lighting/>,
            aboutUs: ()=> <AboutUs/>,
            contactUs: ()=> <ContactUs/>,
            delivery: ()=> <Delivery/>,
            returns: ()=> <Returns/>
        };
        const link = mapTypeToLink[type];
        return link && link();
    }
}



export default LinkFile;