import React, {Component} from 'react';
import styles from './FooterDescription.css';
import IconSVGFile from '../../../../IconSVGFile';
import LinkFile from "./LinkFile";


class FooterDescription extends Component {

    render() {
        return(
            <div className="description">
                <div className="description__text">SOHO make it possible for everyone to create a unique and personal look by introducing a large
                    range of different fabrics, lovely patterns and beautiful colors. Thank your for your attention.
                </div>
                <div className="description__linkBlock">
                    <div className="description__link">
                        <LinkFile type = 'featured'/>
                    </div>
                    <div className="description__link">
                        <LinkFile type = 'furniture'/>
                    </div>
                    <div className="description__link"><LinkFile type = 'decoration'/></div>
                    <div className="description__link"><LinkFile type = 'electronics'/></div>
                    <div className="description__link"><LinkFile type = 'lighting'/></div>
                </div>
                <div className="description__linkBlock2">
                    <div className="description__link"><LinkFile type = 'aboutUs'/></div>
                    <div className="description__link"><LinkFile type = 'contactUs'/></div>
                    <div className="description__link"><LinkFile type = 'delivery'/></div>
                    <div className="description__link"><LinkFile type = 'returns'/></div>
                </div>
                <div className="copyright">
                    <div className="copyright_text">Copyright 2016 Soho Theme.</div>
                    <div className="copyright_text text">Powered by</div>
                    <a className="copyright_text text underline">Shopify</a>
                    <div className="copyright_paymentSystem">
                        <div className="copyright__icon visa"><IconSVGFile type = 'visa'/></div>
                        <div className="copyright__icon masterCard"><IconSVGFile type = 'masterCard'/></div>
                        <div className="copyright__icon payPal"><IconSVGFile type = 'payPal'/></div>

                    </div>
                </div>
            </div>
        )
    }
}

export default FooterDescription;