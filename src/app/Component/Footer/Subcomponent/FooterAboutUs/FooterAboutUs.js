import React, {Component} from 'react';
import styles from './FooterAboutUs.css';

class FooterAboutUs extends Component {

    render() {
        return(
            <div className="aboutUs">
                <div className="aboutUs__text">About Us</div>
            </div>
        )
    }
}

export default FooterAboutUs;