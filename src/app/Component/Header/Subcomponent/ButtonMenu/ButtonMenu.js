import React, {Component} from 'react';
import styles from './ButtonMenu.css';
import IconSVGFile from '../../../../IconSVGFile';

class ButtonMenu extends Component {

    render() {
        return(
            <div className="header__buttonMenu header__border">
                <button className="header__menuButton">
                    <IconSVGFile type = 'buttonMenu'/>
                </button>
            </div>
        )
    }
}

export default ButtonMenu;