import React, {Component} from 'react';
import styles from './HeaderLogo.css';
import { Link } from 'react-router-dom';

class HeaderLogo extends Component {

    render() {
        return(
            <div className="header__logo">
                <Link to = "/" className="header__logo">
                soho
                </Link>
            </div>
        );
    }
}

export default HeaderLogo;