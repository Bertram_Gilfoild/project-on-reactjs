import React, {Component} from 'react';
import styles from './HeaderButton.css';
import IconSVGFile from '../../../../IconSVGFile';

class HeaderButton extends Component {

    render() {
        return(
            <div className="header__button header__menu">
                <button className="header__shopping" ><IconSVGFile type = 'headerButton'/></button>
            </div>
        )
    }
}

export default HeaderButton;