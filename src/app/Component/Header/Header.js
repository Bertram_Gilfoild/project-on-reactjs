import React, {Component} from 'react';
import styles from './Header.css';
import ButtonMenu from './Subcomponent/ButtonMenu/ButtonMenu';
import HeaderButton from './Subcomponent/HeaderButton/HeaderButton';
import HeaderLogo from './Subcomponent/HeaderLogo/HeaderLogo';
import { Link } from 'react-router-dom';

class Header extends Component {

    render() {
        return(
            <div className="header">
                <ButtonMenu/>
                <HeaderLogo/>
                <HeaderButton/>
            </div>
        );
    }
}

export default Header;