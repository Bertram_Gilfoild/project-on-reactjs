import React, {Component} from 'react';
import styles from './SubscribeBlockSocailNetwork.css';
import IconSVGFile from '../../../../IconSVGFile'

//массив, хранящий типы иконок
const mapIcon = [
    'facebook',
    'twitter',
    'instagram'
];

class SubscribeBlockSocialNetwork extends Component {



    render() {

        return(
            <div className="socialNetwork">
                <div className="socialNetwork__button">
                    {mapIcon.map((item, index) => <button key={index} className={"socialNetwork__" + item + ' button'}><IconSVGFile type = {item}/></button> )}
                </div>
        </div>
        )
    }
}

export default SubscribeBlockSocialNetwork;