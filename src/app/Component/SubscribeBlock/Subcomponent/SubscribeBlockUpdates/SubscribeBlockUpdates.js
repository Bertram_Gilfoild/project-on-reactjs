import React, {Component} from 'react';
import styles from './SubscribeBlockUpdates.css';

class SubscribeBlockUpdates extends Component {


    render() {
        return(
            <div className="updates">
                <div className="updates__text">Subscribe to get <b>updates</b> </div>
                <input type="text" placeholder="Your e-mail..." className="updates__input"/>
                <button className="updates__button">Subscribe</button>

            </div>
        )

    }
}

export default SubscribeBlockUpdates;