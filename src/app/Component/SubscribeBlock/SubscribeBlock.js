import React, {Component} from 'react';
import styles from './SubscribeBlock.css';
import SubscribeBlockUpdates from "./Subcomponent/SubscribeBlockUpdates/SubscribeBlockUpdates";
import SubscribeBlockSocialNetwork from "./Subcomponent/SubscribeBlockSocialNetwork/SubscribeBlockSocialNetwork";
import IconSVGFile from '../../IconSVGFile';


class SubscribeBlock extends Component {

    render() {
        return(
            <div className="subscribeBlock">
                <SubscribeBlockSocialNetwork/>
                <SubscribeBlockUpdates/>
            </div>
        );
    }
}


export default SubscribeBlock;