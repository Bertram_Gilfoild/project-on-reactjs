import React, {Component} from 'react';
import styles from './Slide.css';

class slide extends Component {

    render() {

        return(

            <div className="slide">
                <div className="slide__textBlock">
                    <div className="slide__title">{this.props.item.title}</div>
                    <div className="slide__price">${this.props.item.price}</div>
                    <a className="slide__buy">{this.props.item.buy}</a>
                </div>
                <div className="slide__imageBlock" style={{backgroundImage: `url(${this.props.item.image})`}}/>

            </div>
    )
    }
}

export default slide;