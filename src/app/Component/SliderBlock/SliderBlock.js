import React, {Component} from 'react';
import styles from './SliderBlock.css';
import Slide from './Subcomponent/Slide';
import img from '../../../images/product1.jpg';
import cx from 'classnames';


class SliderBlock extends Component {
    constructor(...args){
        super(...args);
        this.state = {activeSlide: 0};
    }

    handleOnClick(index) {
        this.setState({activeSlide:index});
    }


    render() {
        const{activeSlide} = this.state;

        return (
            <div className="sliderBlock">
                <div className="slideContainer">
                    <div className="slideBlock" style ={{transform: `translate3d(${-activeSlide *100}%, 0, 0)`}}>
                    {this.props.items.map((item, index) => <Slide key={index} item={item}/>)}
                    </div>

                    <div className="slide__buttons">
                    {this.props.items.map((item, index) => <button key={index} onClick={this.handleOnClick.bind(this, index)} className= {cx('slide__button',
                        {'slide__buttonActive':
                            activeSlide===index
                        })}
                        />)}
                    </div>
                </div>
            </div>
        )
    }
}

export default SliderBlock;