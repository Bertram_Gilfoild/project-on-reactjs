import React, {Component} from 'react';
import styles from './DiscountCard.css';

class DiscountCard extends Component {
    render() {
        return(
            <div className="discountCard">

                <div className="discountCard__salePercent">
                    <div className="discountCard__sale">{this.props.item.sale}</div>
                    <div className="discountCard__percent">{this.props.item.percent}</div>
                </div>
                <div className="discountCard__title">{this.props.item.title}</div>
                <div className="discountCard__description">{this.props.item.description}</div>

            </div>
        )
    }
}

export default DiscountCard;