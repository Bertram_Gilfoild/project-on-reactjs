import React, {Component} from 'react';
import styles from './SpecialOfferCard.css';
import { Link } from 'react-router-dom';

class SpecialOfferCard extends Component {

    render() {
        return(
            <div className="specialOfferCard">
                <div className="specialOfferCard__special" style={{backgroundImage: `url(${this.props.item.image})`}}>
                    <p className="special">{this.props.item.special}</p>
                </div>
                <div className="specialOfferCard__title">{this.props.item.title}</div>
                <div className="specialOfferCard__newPrice">${this.props.item.newprice}</div>
                <div className="specialOfferCard__oldPrice">${this.props.item.oldprice}</div>
            </div>
        )
    }
}

export default SpecialOfferCard;