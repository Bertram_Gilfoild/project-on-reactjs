import React, {Component} from 'react';
import styles from './CardProduct.css';
import { Link } from 'react-router-dom';


class CardProduct1 extends Component {

    render() {

        return(

            <div className="cardProduct">
                <div className="cardProduct__image" style={{backgroundImage: `url(${this.props.item.image})`}}/>

                <div className="cardProduct__title">{this.props.item.title}</div>

                <div className="cardProduct__description"><Link to="/product/1" className="cardProduct__description">{this.props.item.description}</Link></div>

                <div className="cardProduct__price">${this.props.item.price}</div>
            </div>
        );
    }
}

export default CardProduct1;