import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styles from './CardBlock.css';
import CardProduct from "./Subcomponent/CardProduct/CardProduct";
import img1 from "../../../images/product_3.png";
import img2 from "../../../images/product_4.png";
import img3 from "../../../images/product_5.png";
import img4 from "../../../images/product_6.png";
import img5 from "../../../images/product_7.png";
import img6 from "../../../images/product_8.png";
import img7 from "../../../images/product_9.png";
import img8 from "../../../images/woman_1.jpg";
import SpecialOfferCard from "./Subcomponent/SpecialOfferCard/SpecialOfferCard";
import DiscountCard from "./Subcomponent/DiscountCard/DiscountCard";



class CardBlock extends Component {


    render() {

        return(
            <div className="cardBlock">

                { this.props.items.map((array_item, index) => {
                    if (array_item.type === 'product'){
                        return <CardProduct key={index} item={array_item}/>
                    }
                    if (array_item.type === 'special'){
                        return <SpecialOfferCard key={index} item={array_item}/>
                    }
                    if (array_item.type === 'discount'){
                        return <DiscountCard key={index} item={array_item}/>
                    }
                    return null;
                   })}
            </div>
        )
    }
}



export default CardBlock;