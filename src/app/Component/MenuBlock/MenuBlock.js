import React, {Component} from 'react';
import styles from './MenuBlock.css';
import IconSVGFile from '../../IconSVGFile';
import cx from 'classnames';



class MenuBlock extends Component {

    constructor(...args){
        super(...args);
        this.state ={
            show: false,
            activeList:0
        }; //состояние раскрывающегося списка до клика
        this.showList = this.showList.bind(this);

    }

    showList(){
        this.setState({
            show: !this.state.show,
        }) ; //состояние раскрывающегося списка после клика
    }

    activeItems(index){
        this.setState({ activeList: index, show: false});

    }

    render() {
        const{show} = this.state;
        const {activeList} = this.state;
        return(
            <div className='menuBlock'>
                <ul className={cx('menuLink',{'click': show})}> {/* тут будет назначаться класс списка и добавочный класс отвечающий за открытие раскрывашки*/}
                    {/* <ol className="onlyMobile active">1 </ol>одержит активное выбранное значение и показывается только в мобилке,
                    на клик по этому элементу нужно менять переменную в state, по которой мы меняем класс у ul элемента  */}
                    <p className="category">Category:</p>

                    <li onClick={this.showList} className='menuBlock_link'>{this.props.items[activeList].title}</li>

                    {this.props.items.map((item, index)=>

                        <li href={item.link} key={index} onClick={this.activeItems.bind(this, index)}
                            className={cx("menuBlock_link",
                                {'active':
                                activeList === index
                                })}>
                            {item.title}
                        </li> )}
                    <button className="categoryButton"><IconSVGFile type = 'category'/></button>
                </ul>
            </div>

        )

    }
}

export default MenuBlock;

