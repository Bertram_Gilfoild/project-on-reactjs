import React, {Component} from 'react';
import Header from './Component/Header/Header';
import styles from './App.css';
import SliderBlock from "./Component/SliderBlock/SliderBlock";
import MenuBlock from "./Component/MenuBlock/MenuBlock";
import CardBlock from "./Component/CardBlock/CardBlock";
import ShowMoreBlock from "./Component/ShowMoreBlock/ShowMoreBlock";
import Footer from "./Component/Footer/Footer";
import SubscribeBlock from "./Component/SubscribeBlock/SubscribeBlock";
import { BrowserRouter,  Link, Route } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import InfoProduct from "./ProductPage/InfoProduct/InfoProduct";
import ProductCard from "./ProductPage/ProductCard/ProductCard";
import axios from 'axios';
import PropTypes from 'prop-types';
import product1 from "../images/preview_1.jpg";
import product2 from "../images/preview_2.jpg";
import product3 from "../images/preview_3.jpg";
import product4 from "../images/preview_4.jpg";


const history = createHistory();

const imageProduct = [
    {
        type: 'orange',
        photo1: product1
    },
    {
        type: 'grey',
        photo2: product2
    },
    {
        type: 'black',
        photo3: product3
    },
    {
        type: 'blue',
        photo4: product4
    }
];

const menu = [
    {
        link: '/featured',
        title:'Featured'
    },
    {
        link: '/furniture',
        title:'Furniture'
    },
    {
        link: '/decoration',
        title:'Decoration'
    },
    {
        link: '/lighting',
        title:'Lighting'
    },
    {
        link: '/electronics',
        title:'Electronics'
    }

];

class ProductPage extends Component{

    render(){
        console.log('id', this.props.match.params.id);
        return(
            <div>
                <Header/>
                <InfoProduct/>
                <ProductCard id = {this.props.match.params.id} items = {imageProduct}/>
            </div>
        )
    }
}

class IndexPage extends  Component{
    constructor(props){
        super(props);

        this.state = { //инициализируем состояние
            cards: null, //получаем пустой объект posts
            slides: null
        };

    }
    componentDidMount(){ //делаем запрос
        axios.get('http://www.mocky.io/v2/5a0b50b93200004f0ae963f3')
            .then(res => {
                const cards = res.data.items; //срабатывает
                const slides = res.data.feature;
                this.setState({cards, slides}); //обновляем состояние объекта
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    render(){
        return (
            <div>
                <Header/>
                <SliderBlock items = {this.state.slides || []} />
                <MenuBlock items={menu}/>
                <CardBlock items = { this.state.cards || []} />
                <ShowMoreBlock/>
            </div>
            )
    }
}


class App extends Component {
    render() {
        return(
            <div>

                <BrowserRouter>
                    <div>
                        <Route exact path = "/" component={IndexPage}/>
                        <Route path = "/product/:id" component={ProductPage}/>
                    </div>
                </BrowserRouter>
                <SubscribeBlock/>
                <Footer/>
            </div>


        );

    }
}

export default App;


