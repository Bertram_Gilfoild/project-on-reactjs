import React, {Component, PropTypes as T} from 'react';
import styles from './ProductCharacteristics.css';

class ProductCharacteristics extends Component {


    render() {
        return(
            <div className="infoProduct__char">
                <div className="char1">
                    <div className="char__string"><b>Dimensions:</b> 26.8 cm / 9 cm / 36.2 cm</div>
                    <div className="char__string"><b>Weight:</b> 4.65 kg (without packaging)</div>
                    <div className="char__string"><b>Material:</b> Wool, Aluminum</div>
                    <div className="char__string"> <b>Colors:</b> Light Grey, Red, Black</div>

                </div>
                <div className="char2">
                    <div className="char__string"><b>Signal transmission: </b>Wireless</div>
                    <div className="char__string"><b>Connections:</b> 3.5 mm Mini-Klinke</div>
                    <div className="char__string"><b>Battery capacity: </b>2600 mAh</div>
                    <div className="char__string"><b>Maximum power: </b>150 Watt</div>
                </div>
            </div>
        )

    }
}

export default ProductCharacteristics;