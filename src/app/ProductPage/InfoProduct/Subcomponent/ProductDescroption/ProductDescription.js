import React, {Component, PropTypes as T} from 'react';
import styles from './ProductDescriptions.css';

class ProductDescription extends Component {


    render() {
        return(
            <div className="infoProduct__description">Creating an authentic sound - is a question of orchestration. Only experience,
                love for music and an uncompromising position in matters of quality produces an authentic sound.
                This was always the philosophy of Vifa and the reason is that quality music and design appreciate  the Copenhagen.
            </div>
        )

    }
}

export default ProductDescription;