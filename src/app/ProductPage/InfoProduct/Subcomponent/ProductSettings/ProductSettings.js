import React, {Component, PropTypes as T} from 'react';
import styles from './ProductSettings.css';

class ProductSettings extends Component {


    render() {
        return(
            <div className="infoProduct__settings">
                <div className="category__color">
                    <select >
                        <option>Color: Light Grey</option>
                    </select>
                </div>

                <input type="text" placeholder="1" className="category__quantity"/>
                <button className="button__add">Add to Cart</button>

            </div>

        )

    }
}

export default ProductSettings;