import React, {Component, PropTypes as T} from 'react';
import styles from './ProductPhoto.css';

class ProductPhoto extends Component {


    render() {
        return (
            <div className="infoProduct__photo">
                <div className="back">Back to Electronics</div>
                <div className="photo"/>
                <div className="link__previousNext">
                    <div className="previous">Previous</div>
                    <div className="next">Next</div>
                </div>
                <div className="infoProduct__title">
                    <p className="title">Braun Watch 1223</p>
                    <p className="price">$399</p>
                </div>
            </div>
        )

    }
}

export default ProductPhoto;