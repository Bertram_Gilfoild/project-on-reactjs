import React, {Component, PropTypes as T} from 'react';
import styles from './InfoProduct.css';
import ProductPhoto from "./Subcomponent/ProductPhoto/ProductPhoto";
import ProductSettings from "./Subcomponent/ProductSettings/ProductSettings";
import ProductDescription from "./Subcomponent/ProductDescroption/ProductDescription";
import ProductCharacteristics from "./Subcomponent/ProductCharacteristics/ProductCharacteristics";



class InfoProduct extends Component {


    render() {
        return(
            <div className="info__product">
                <ProductPhoto/>
                <ProductSettings/>
                <ProductDescription/>
                <ProductCharacteristics/>
            </div>
        )

    }
}

export default InfoProduct;