import React, {Component, PropTypes as T} from 'react';
import styles from './ProductCard.css';

class ProductCard extends Component {


    render() {

        return(
            <div className="productCard">
                {this.props.items.map((item, index) => {
                    if (item.type === 'orange'){
                        return <div className="productCard__photo" style={{backgroundImage: `url(${item.photo1})`}} key = {index}/>}
                    if (item.type === 'grey'){
                        return <div className="productCard__photo" style={{backgroundImage: `url(${item.photo2})`}} key = {index}/>
                    }
                    if (item.type === 'black'){
                        return <div className="productCard__photo black" style={{backgroundImage: `url(${item.photo3})`}} key = {index}/>
                    }
                    if (item.type === 'blue'){
                        return <div className="productCard__photo blue" style={{backgroundImage: `url(${item.photo4})`}} key = {index}/>
                    }
                })}

            </div>
        )

    }
}

export default ProductCard;